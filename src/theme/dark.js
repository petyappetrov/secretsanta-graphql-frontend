/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

const dark = {
  text: '#fff',
  backgroundColor: '#000',
  logo: '#fff',
  buttons: {
    secondary: {
      text: '#000',
      bg: '#fff',
      hover: '#fff'
    }
  }
}

export default dark
