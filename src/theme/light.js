/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

const light = {
  text: '#000',
  backgroundColor: '#fff',
  logo: '#CF4F5A',
  buttons: {
    secondary: {
      text: '#fff',
      bg: '#000',
      hover: '#333'
    }
  }
}

export default light
