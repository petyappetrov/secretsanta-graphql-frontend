/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

export const get = (obj, path) => {
  if (path.includes('.')) {
    return path.split('.').reduce((i, key) => i[key], obj)
  }
  return obj[path]
}
