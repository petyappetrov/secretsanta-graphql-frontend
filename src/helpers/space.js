/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

const REG = /^[mp][trblxy]?$/
const scale = [0, 4, 8, 16, 32, 64]

const properties = {
  m: 'margin',
  p: 'padding'
}

const directions = {
  t: '-top',
  r: '-right',
  b: '-bottom',
  l: '-left',
  x: ['-left', '-right'],
  y: ['-top', '-bottom']
}

const getProperties = key => {
  const [a, b] = key.split('')
  const property = properties[a]
  const direction = directions[b] || ''
  return Array.isArray(direction)
    ? direction.map(dir => property + dir)
    : [property + direction]
}

const getSpace = (index) => {
  if (typeof index === 'number') {
    return scale[index] + 'px;'
  }
  return index
}

const createStyles = props => key => {
  const value = props[key]
  const properties = getProperties(key)
  return properties.reduce((prev, prop) => ({ ...prev, [prop]: getSpace(value) }), {})
}

const space = (props) =>
  Object.keys(props)
    .filter(key => REG.test(key))
    .sort()
    .map(createStyles(props))

export default space
