/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import express from 'express'
import webpack from 'webpack'
import webpackDevMiddleware from 'webpack-dev-middleware'
import webpackHotMiddleware from 'webpack-hot-middleware'
import path from 'path'
import config from '../webpack/config.dev'

const app = express()
app.use(express.static(path.join(__dirname, '..', 'dist')))

if (process.env.NODE_ENV === 'development') {
  const compiler = webpack(config)
  app.use(webpackDevMiddleware(compiler, {
    hot: true,
    stats: {
      children: false,
      colors: true,
      hash: false,
      timings: false,
      chunks: false,
      chunkModules: false,
      modules: false
    }
  }))
  app.use(webpackHotMiddleware(compiler))
}

app.get('*', (req, res) => res.sendFile(path.join(__dirname, '..', 'dist/index.html')))

app.listen(process.env.PORT, (err) => {
  if (err) {
    throw new Error(err)
  }
  console.log(`Server started on http://localhost:${process.env.PORT}`)
})
