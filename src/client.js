/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import { render } from 'react-dom'
import { ApolloProvider } from 'react-apollo'
import { ApolloProvider as ApolloHooksProvider } from 'react-apollo-hooks'
import { setContext } from 'apollo-link-context'
import { ApolloClient } from 'apollo-boost'
import { InMemoryCache, IntrospectionFragmentMatcher } from 'apollo-cache-inmemory'
import { split } from 'apollo-link'
import { HttpLink } from 'apollo-link-http'
import { WebSocketLink } from 'apollo-link-ws'
import { getMainDefinition } from 'apollo-utilities'
import SettingsQuery from 'schemas/queries/SettingsQuery'
import introspectionQueryResultData from './fragment-types.json'
import App from './Application'

const httpLink = new HttpLink({
  uri: process.env.GRAPHQL_URI + '/graphql'
})

const wsLink = new WebSocketLink({
  uri: process.env.GRAPHQL_WS,
  options: {
    reconnect: true,
    connectionParams: () => {
      const token = localStorage.getItem('jwt')
      return {
        authToken: token ? `Bearer ${token}` : ''
      }
    }
  }
})

const authLink = setContext((root, { headers }) => {
  const token = localStorage.getItem('jwt')
  return {
    headers: {
      ...headers,
      Authorization: token ? `Bearer ${token}` : ''
    }
  }
})

const fragmentMatcher = new IntrospectionFragmentMatcher({
  introspectionQueryResultData
})

const link = split(
  ({ query }) => {
    const { kind, operation } = getMainDefinition(query)
    return kind === 'OperationDefinition' && operation === 'subscription'
  },
  wsLink,
  authLink.concat(httpLink)
)

/**
 * Initialize cache
 */
const cache = new InMemoryCache({ fragmentMatcher })

/**
 * Locale resolvers for theme and language
 */
const resolvers = {
  Mutation: {
    updateSettings: (root, { theme, locale }, { cache }) => {
      const data = cache.readQuery({ query: SettingsQuery })
      const settings = data.settings
      if (locale) {
        settings.locale = locale
      }
      if (theme) {
        settings.theme = theme
      }
      cache.writeData({ data: { settings } })
    }
  }
}

/**
 * Create Apollo store
 */
const client = new ApolloClient({
  link,
  cache,
  connectToDevTools: true,
  resolvers
})

/**
 * Initial data for Apollo cache
 */
const data = {
  settings: {
    __typename: 'Settings',
    theme: 'light',
    locale: navigator.language.split('-')[0]
  }
}

/**
 * Write inital data to cache
 */
cache.writeData({ data })

/**
 * Update data after login or logout
 */
client.onResetStore(() => cache.writeData({ data }))

render(
  <ApolloProvider client={client}>
    <ApolloHooksProvider client={client}>
      <App />
    </ApolloHooksProvider>
  </ApolloProvider>,
  document.getElementById('root')
)
