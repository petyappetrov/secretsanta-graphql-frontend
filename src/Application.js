/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React, { useEffect } from 'react'
import { hot } from 'react-hot-loader/root'
import { Router } from '@reach/router'
import { ThemeProvider } from 'styled-components'
import { I18nProvider } from '@lingui/react'
import { useQuery, useMutation } from 'react-apollo-hooks'
import {
  HomePage,
  ProfilePage,
  LoginPage,
  RegisterPage,
  RoomPage,
  CreateRoomPage,
  RoomsPage,
  NotificationsPage,
  Spinner,
  Notifications,
  ConversationsPage,
  Header,
  Footer
} from 'ui'
import MeQuery from 'schemas/queries/MeQuery'
import SettingsQuery from 'schemas/queries/SettingsQuery'
import UpdateSettginsMutation from 'schemas/mutations/UpdateSettginsMutation'
import GlobalStyles from 'theme/GlobalStyles'
import * as themes from 'theme'
import catalogEn from './locales/en/messages'
import catalogRu from './locales/ru/messages'
/* eslint-disable react-hooks/exhaustive-deps */

const catalogs = {
  en: catalogEn,
  ru: catalogRu
}

const Application = () => {
  const { data, loading } = useQuery(MeQuery)
  const { data: { settings } } = useQuery(SettingsQuery)
  const updateSettings = useMutation(UpdateSettginsMutation)

  /**
   * Setting up settings for user
   */
  useEffect(() => {
    if (data.me) {
      updateSettings({
        variables: {
          theme: data.me.theme,
          locale: data.me.locale
        }
      })
    }
  }, [data.me])

  if (loading) {
    return <Spinner />
  }

  return (
    <I18nProvider language={settings.locale} catalogs={catalogs}>
      <ThemeProvider theme={themes[settings.theme]}>
        <>
          <GlobalStyles />
          <Header />
          <Router primary={false}>
            <HomePage path='/' me={data.me} />
            <ProfilePage path='profile' me={data.me} />
            <LoginPage path='login' me={data.me} />
            <RegisterPage path='register' me={data.me} />
            <CreateRoomPage path='create-room' me={data.me} />
            <RoomsPage path='rooms' me={data.me} />
            <NotificationsPage path='notifications' me={data.me} />
            <ConversationsPage path='conversations' me={data.me} />
            <RoomPage path=':slug' me={data.me} />
          </Router>
          <Footer />
          {data.me && <Notifications me={data.me} />}
        </>
      </ThemeProvider>
    </I18nProvider>
  )
}

export default hot(Application)
