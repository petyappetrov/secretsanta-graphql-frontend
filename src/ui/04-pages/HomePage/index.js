/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import { Link } from '@reach/router'
import { Trans } from '@lingui/macro'
import styled from 'styled-components'
import { PageTemplate, Section, Table, Button, Text } from 'ui'
import imagePath from './bg.jpg'

const Hero = styled.div`
  height: 300px;
  width: 100%;
  background-image: url(${imagePath});
  background-size: cover;
  margin-bottom: 2rem;
  box-sizing: border-box;
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  overflow: hidden;
  border-radius: 15px;
  &::before {
    content: '';
    position: absolute;
    left: 0; right: 0;
    top: 0; bottom: 0;
    background-color: #000;
    opacity: .35;
  }
`

const HeroInnerBlock = styled.div`
  text-align: center;
  padding: 0 80px;
  color: #fff;
  position: relative;
`

const HeroText = styled(Text)`
  font-size: 18px;
  font-weight: bold;
  margin-top: 1rem;
  margin-bottom: 1.5rem;
`

const HomePage = ({ me }) =>
  <PageTemplate>
    <Hero>
      <HeroInnerBlock>
        <HeroText>
          <Trans>Организуй тайный обмен подарками между друзьями, членами семьи или коллегами!</Trans>
        </HeroText>
        <Button to='/create-room'>
          <Trans>Организовать игру</Trans>
        </Button>
      </HeroInnerBlock>
    </Hero>
    {me && (
      <Section title={<Trans>Мои последние комнаты</Trans>} withToggler>
        <Table
          data={me.rooms}
          columns={[
            {
              header: <Trans>Дата</Trans>,
              accessor: 'createdAt',
              width: 120
            },
            {
              header: <Trans>Название</Trans>,
              accessor: 'name',
              transform: (col, row) => <Link to={`/${row.slug}`}>{col}</Link>
            },
            {
              header: <Trans>Организатор</Trans>,
              accessor: 'owner.name',
              width: 160
            },
            {
              header: <Trans>Статус</Trans>,
              accessor: 'isPlayed',
              width: 120,
              transform: (col) => col
                ? <Trans>Разыграна</Trans>
                : <Trans>Не разыграна</Trans>
            }
          ]}
        />
        <Text center>
          <Button
            view='secondary'
            mt={3}
            to='/rooms'
          >
            <Trans>Все комнаты</Trans>
          </Button>
        </Text>
      </Section>
    )}
  </PageTemplate>

HomePage.propTypes = {
  me: PropTypes.object
}

export default HomePage
