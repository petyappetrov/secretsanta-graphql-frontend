/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { Query } from 'react-apollo'
import { Redirect, Link } from '@reach/router'
import { Trans } from '@lingui/macro'
import { PageTemplate, Section, Spinner, Avatar, Text } from 'ui'
import ConversationsQuery from 'schemas/queries/ConversationsQuery'

const Message = styled(Link)`
  display: flex;
  color: ${p => p.theme.text};
  &:not(:last-child) {
    margin-bottom: 2rem;
  }
  &:hover {
    text-decoration: none;
  }
`
const MessageAvatar = styled.div`
  width: 70px;
  flex-shrink: 0;
`
const MessageText = styled.div`
`
const MessageGroup = styled.div`
  font-size: 18px;
  font-weight: bold;
  margin-bottom: .5rem;
`
const MessageDate = styled.div`
  margin-left: auto;
  flex-shrink: 0;
  opacity: 0.8;
`

const ConversationsPage = ({ me }) => {
  if (!me) {
    return <Redirect to='/login' noThrow />
  }
  return (
    <PageTemplate>
      <Query query={ConversationsQuery}>
        {({ data, loading, fetchMore }) => {
          if (loading) {
            return <Spinner />
          }
          return (
            <Section title={<Trans>Сообщения</Trans>}>
              {data.conversations.map(conversation => {
                if (conversation.messages.count === 0) {
                  return null
                }
                return (
                  <Message key={conversation._id} to={`/${conversation.room.slug}`}>
                    <MessageAvatar>
                      <Avatar alt={conversation.messages.items[0].author.name} size={60} />
                    </MessageAvatar>
                    <MessageText>
                      <MessageGroup>{conversation.room.name}</MessageGroup>
                      <Text style={{ opacity: 0.8 }}>
                        {conversation.messages.items[0].author._id === me._id
                          ? <Trans>Вы</Trans>
                          : conversation.messages.items[0].author.name}:{' '}
                        {conversation.messages.items[0].text}
                      </Text>
                    </MessageText>
                    <MessageDate>
                      {conversation.messages.items[0].createdAt}
                    </MessageDate>
                  </Message>
                )
              })}
            </Section>
          )
        }}
      </Query>
    </PageTemplate>
  )
}

ConversationsPage.propTypes = {
  me: PropTypes.object
}

export default ConversationsPage
