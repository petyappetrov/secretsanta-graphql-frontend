/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import { useQuery } from 'react-apollo-hooks'
import { Redirect, Link } from '@reach/router'
import { Trans } from '@lingui/macro'
import { PageTemplate, Section, Table, Spinner } from 'ui'
import RoomsQuery from 'schemas/queries/RoomsQuery'

const RoomsPage = ({ me }) => {
  const { data, loading } = useQuery(RoomsQuery)
  if (!me) {
    return <Redirect to='/login' noThrow />
  }
  if (loading) {
    return <Spinner />
  }
  return (
    <PageTemplate>
      <Section title={<Trans>Мои комнаты</Trans>}>
        <Table
          data={data.rooms}
          columns={[
            {
              header: <Trans>Дата</Trans>,
              accessor: 'createdAt',
              width: 120
            },
            {
              header: <Trans>Название</Trans>,
              accessor: 'name',
              transform: (col, row) => <Link to={`/${row.slug}`}>{col}</Link>
            },
            {
              header: <Trans>Организатор</Trans>,
              accessor: 'owner.name',
              width: 160
            },
            {
              header: <Trans>Статус</Trans>,
              accessor: 'isPlayed',
              width: 120,
              transform: (col) => col
                ? <Trans>Разыграна</Trans>
                : <Trans>Не разыграна</Trans>
            }
          ]}
        />
      </Section>
    </PageTemplate>
  )
}

RoomsPage.propTypes = {
  me: PropTypes.object
}

export default RoomsPage
