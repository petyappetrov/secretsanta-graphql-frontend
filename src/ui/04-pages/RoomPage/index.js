/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import { Query, Mutation } from 'react-apollo'
import { Redirect } from '@reach/router'
import { Trans } from '@lingui/macro'
import RoomQuery from 'schemas/queries/RoomQuery'
import WishQuery from 'schemas/queries/WishQuery'
import MakeTossMutation from 'schemas/mutations/MakeTossMutation'
import {
  PageTemplate,
  Spinner,
  Button,
  Conversation,
  Section,
  Box,
  Gradient,
  Modal,
  WishForm,
  Textarea,
  Label
} from 'ui'

const RoomPage = ({ me, slug, ...props }) => {
  if (!me) {
    return <Redirect to={`/register?room=${slug}`} noThrow />
  }
  return (
    <PageTemplate>
      <Query
        query={RoomQuery}
        variables={{ slug }}
      >
        {({ loading, data }) => {
          if (loading) {
            return <Spinner />
          }
          if (!data) {
            return null
          }
          const { room } = data
          return (
            <>
              <Gradient>
                <Gradient.Title>{room.name}</Gradient.Title>
                <Modal>
                  {({ openModal, closeModal, content }) =>
                    <>
                      <Button onClick={openModal} mt={3} view='optional' mr={2}>
                        {room.meHave
                          ? <Trans>Моё пожелание</Trans>
                          : <Trans>Участвовать</Trans>
                        }
                      </Button>
                      {content(
                        <Section
                          title={room.meHave
                            ? <Trans>Моё пожелание</Trans>
                            : <Trans>Участвовать</Trans>
                          }
                        >
                          <WishForm
                            room={room._id}
                            slug={slug}
                            wish={room.myWish}
                            onCancel={closeModal}
                          />
                        </Section>
                      )}
                    </>
                  }
                </Modal>
                {room.isPlayed && (
                  <Modal>
                    {({ openModal, closeModal, content }) =>
                      <>
                        <Button mt={3} onClick={openModal} view='optional'>
                          Мой подопечный
                        </Button>
                        {content(
                          <Query
                            query={WishQuery}
                            fetchPolicy='cache-and-network'
                            variables={{ room: room._id }}
                          >
                            {({ data, loading }) => {
                              if (loading && (data && !data.wish)) {
                                return <Spinner />
                              }
                              return (
                                <Section title={<Trans>Мой подопечный {data.wish.author.name}</Trans>}>
                                  <Label>Пожелание к подарку</Label>
                                  <Textarea value={data.wish.text} readOnly />
                                  <Box mt={5}>
                                    <Button view='secondary' mr={2} onClick={closeModal}>
                                      <Trans>Закрыть</Trans>
                                    </Button>

                                    {/* TODO */}
                                    <Button onClick={closeModal}>
                                      <Trans>Написать анонимное сообщение</Trans>
                                    </Button>

                                  </Box>
                                </Section>
                              )
                            }}
                          </Query>
                        )}
                      </>
                    }
                  </Modal>
                )}
                {!room.isPlayed && room.isMyRoom && room.members.length > 2 &&
                  <Mutation
                    mutation={MakeTossMutation}
                    variables={{ room: room._id }}
                    update={(client) => {
                      const data = client.readQuery({
                        query: RoomQuery,
                        variables: { slug }
                      })
                      client.writeQuery({
                        query: RoomQuery,
                        variables: { slug },
                        data: {
                          room: {
                            ...data.room,
                            isPlayed: true
                          }
                        }
                      })
                    }}
                  >
                    {(mutate) =>
                      <Button mt={3} view='optional' onClick={mutate}>
                        <Trans>Разыграть комнату</Trans>
                      </Button>
                    }
                  </Mutation>
                }
              </Gradient>
              <Section title='Участники' withToggler>
                {room.members.length
                  ? room.members.map(member =>
                    <Box mb={2} key={member._id}>
                      <strong>{member.name}</strong>
                      {member._id === room.owner._id && <span>{' '}(<Trans>Организатор</Trans>)</span>}
                    </Box>
                  )
                  : <Trans>Пока нет участников</Trans>
                }
              </Section>
              {room.meHave &&
                <Conversation
                  conversation={room.conversation}
                  me={me}
                />
              }
            </>
          )
        }}
      </Query>
    </PageTemplate>
  )
}

RoomPage.propTypes = {
  slug: PropTypes.string,
  me: PropTypes.object
}

export default RoomPage
