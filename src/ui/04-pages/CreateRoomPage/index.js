/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import { Redirect } from '@reach/router'
import { Trans } from '@lingui/macro'
import { PageTemplate, CreateRoomForm, Section } from 'ui'

const CreateRoomPage = ({ me }) => {
  if (!me) {
    return <Redirect to='/login' noThrow />
  }
  return (
    <PageTemplate>
      <Section title={<Trans>Организовать игру</Trans>}>
        <CreateRoomForm />
      </Section>
    </PageTemplate>
  )
}

CreateRoomPage.propTypes = {
  me: PropTypes.object
}

export default CreateRoomPage
