/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import { Redirect } from '@reach/router'
import { Trans } from '@lingui/macro'
import { PageTemplate, RegisterForm, Section } from 'ui'

const RegisterPage = ({ me, ...props }) => {
  if (me) {
    return <Redirect to='/profile' noThrow />
  }
  return (
    <PageTemplate>
      <Section title={<Trans>Регистрация</Trans>}>
        <RegisterForm {...props} />
      </Section>
    </PageTemplate>
  )
}

RegisterPage.propTypes = {
  me: PropTypes.object
}

export default RegisterPage
