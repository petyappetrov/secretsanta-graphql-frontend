/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import { Redirect } from '@reach/router'
import { Trans } from '@lingui/macro'
import { PageTemplate, LoginForm, Section } from 'ui'

const LoginPage = ({ me }) => {
  if (me) {
    return <Redirect to='/profile' noThrow />
  }
  return (
    <PageTemplate>
      <Section title={<Trans>Вход</Trans>}>
        <LoginForm />
      </Section>
    </PageTemplate>
  )
}

LoginPage.propTypes = {
  me: PropTypes.object
}

export default LoginPage
