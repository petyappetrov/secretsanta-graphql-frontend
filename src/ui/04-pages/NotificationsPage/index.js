/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import { Redirect } from '@reach/router'
import { Trans } from '@lingui/macro'
import { useQuery } from 'react-apollo-hooks'
import { PageTemplate, Section, Spinner, Table, NotificationItem, Button, Text } from 'ui'
import NotificationsQuery from 'schemas/queries/NotificationsQuery'

const NotificationsPage = ({ me }) => {
  const { data, loading, fetchMore } = useQuery(NotificationsQuery, {
    variables: {
      limit: 10,
      skip: 0
    }
  })
  if (!me) {
    return <Redirect to='/' noThrow />
  }
  if (loading && (data && !data.notifications)) {
    return <Spinner />
  }

  return (
    <PageTemplate>
      <Section title={<Trans>Уведомления</Trans>}>
        <Table
          data={data.notifications.items}
          columns={[
            {
              header: <Trans>Дата</Trans>,
              accessor: 'date',
              width: 120
            },
            {
              header: <Trans>Сообщение</Trans>,
              accessor: '__typename',
              transform: (col, row) => {
                const Component = NotificationItem.texts[col]
                if (Component) {
                  return <Component {...row} />
                }
                return null
              }
            },
            {
              header: <Trans>Статус</Trans>,
              accessor: 'isRead',
              width: 120,
              transform: (col) => col
                ? <Trans>Прочитано</Trans>
                : <Trans>Не прочитано</Trans>
            }
          ]}
        />
        {data.notifications.count > data.notifications.items.length && (
          <Text center>
            <Button
              mt={3}
              view='secondary'
              isLoading={loading}
              onClick={() =>
                fetchMore({
                  variables: {
                    skip: data.notifications.items.length
                  },
                  updateQuery: (prev, { fetchMoreResult }) => {
                    if (!fetchMoreResult) {
                      return prev
                    }
                    return {
                      ...prev,
                      notifications: {
                        ...prev.notifications,
                        items: [
                          ...prev.notifications.items,
                          ...fetchMoreResult.notifications.items
                        ]
                      }
                    }
                  }
                })
              }
            >
              <Trans>Загрузить ещё</Trans>
            </Button>
          </Text>
        )}
      </Section>
    </PageTemplate>
  )
}

NotificationsPage.propTypes = {
  me: PropTypes.object
}

export default NotificationsPage
