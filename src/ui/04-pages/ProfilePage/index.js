/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { Redirect } from '@reach/router'
import { Trans } from '@lingui/macro'
import { useApolloClient } from 'react-apollo-hooks'
import { PageTemplate, Section, ProfileEditForm, Box, Avatar } from 'ui'

const ProfilePage = ({ me }) => {
  const client = useApolloClient()
  const [showForm, setForm] = useState(false)
  if (!me) {
    return <Redirect to='/' noThrow />
  }

  const onLogout = () => {
    localStorage.removeItem('jwt')
    client.resetStore()
  }

  return (
    <PageTemplate>
      <Section title={<Trans>Мой профиль</Trans>}>
        {showForm
          ? (
            <ProfileEditForm
              onCancel={() => setForm(false)}
              afterSubmit={() => setForm(false)}
              {...me}
            />
          )
          : (
            <>
              <Box mb={3}>
                <Avatar src={me.avatar} alt={me.name} />
              </Box>
              <Box mb={3}><strong><Trans>Имя</Trans></strong>: {me.name}</Box>
              {me.email &&
                <Box mb={3}><strong><Trans>Почта</Trans></strong>: {me.email}</Box>
              }
              <Box mb={3}>
                <a href='#' onClick={() => setForm(true)}><Trans>Изменить профиль</Trans></a>
              </Box>
              <Box mb={3}>
                <a href='#' onClick={() => setForm(true)}><Trans>Удалить профиль</Trans></a>
              </Box>
              <Box>
                <a href='#' onClick={onLogout}><Trans>Выйти</Trans></a>
              </Box>
            </>
          )
        }
      </Section>
    </PageTemplate>
  )
}

ProfilePage.propTypes = {
  me: PropTypes.object
}

export default ProfilePage
