/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'

const Content = styled.div`
  flex: 1 0 auto;
`

const PageTemplate = ({ children }) =>
  <Content>
    {children}
  </Content>

PageTemplate.propTypes = {
  children: PropTypes.any.isRequired
}

export default PageTemplate
