/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React, { useState, useEffect } from 'react'
import PropTypes from 'prop-types'
import { useSubscription } from 'react-apollo-hooks'
import styled from 'styled-components'
import { useTransition, animated, config } from 'react-spring'
import NotificationPushedSubscription from 'schemas/subscriptions/NotificationPushedSubscription'
import RoomQuery from 'schemas/queries/RoomQuery'

import NotificationMemberSuccessAddedToRoom from './NotificationMemberSuccessAddedToRoom'
import NotificationYouSuccessAddedToRoom from './NotificationYouSuccessAddedToRoom'
import NotificationRoomWasTossed from './NotificationRoomWasTossed'
import NotificationWishHasBeenUpdated from './NotificationWishHasBeenUpdated'
import NotificationRoomSuccessfulCreated from './NotificationRoomSuccessfulCreated'
import NotificationMemberSuccessRegister from './NotificationMemberSuccessRegister'

const updateCache = (notification, client) => {
  switch (notification.__typename) {
    case 'NotificationMemberSuccessAddedToRoom': {
      try {
        const data = client.readQuery({
          query: RoomQuery,
          variables: {
            slug: notification.room.slug
          }
        })
        client.writeQuery({
          query: RoomQuery,
          variables: {
            slug: notification.room.slug
          },
          data: {
            room: {
              ...data.room,
              members: [
                ...data.room.members,
                notification.member
              ]
            }
          }
        })
      } catch (error) {}
      break
    }
    case 'NotificationRoomWasTossed': {
      try {
        const data = client.readQuery({
          query: RoomQuery,
          variables: {
            slug: notification.room.slug
          }
        })
        client.writeQuery({
          query: RoomQuery,
          variables: {
            slug: notification.room.slug
          },
          data: {
            room: {
              ...data.room,
              isPlayed: true
            }
          }
        })
      } catch (error) {}
      break
    }
  }
}

const notificationTexts = {
  NotificationMemberSuccessAddedToRoom,
  NotificationYouSuccessAddedToRoom,
  NotificationRoomWasTossed,
  NotificationWishHasBeenUpdated,
  NotificationRoomSuccessfulCreated,
  NotificationMemberSuccessRegister
}

const NotificationsWrapper = styled.div`
  position: fixed;
  top: 25px;
  right: 25px;
  width: 400px;
  max-height: calc(100% - 30px);
  z-index: 12;
`

const NotificationItemStyled = styled(animated.div)`
  cursor: pointer;
  background-color: ${p => p.theme.backgroundColor};
  color: ${p => p.theme.text};
  border: 1px solid #ebedf0;
  line-height: 1.618;
  font-weight: bold;
  padding: 30px;
  margin-bottom: 10px;
  border-radius: 15px;
`

const NotificationItem = React.forwardRef(({ item, setNotifications, style, delay = 4000 }, ref) => {
  useEffect(() => {
    const timer = setTimeout(() => {
      setNotifications(state => state.filter(({ _id }) => _id !== item._id))
    }, delay)
    return () => clearTimeout(timer)
  }, [])
  const NotificationText = notificationTexts[item.__typename] || null
  return (
    <NotificationItemStyled style={style}>
      <span ref={ref}>
        <NotificationText {...item} />
      </span>
    </NotificationItemStyled>
  )
})

const Notifications = ({ me }) => {
  const [notifications, setNotifications] = useState([])
  const [refMap] = useState(() => new WeakMap())

  useSubscription(NotificationPushedSubscription, {
    onSubscriptionData: ({ subscriptionData, client }) => {
      updateCache(subscriptionData.data.notificationPushed, client)
      setNotifications([...notifications, subscriptionData.data.notificationPushed])
    }
  })

  const transitions = useTransition(notifications, (item) => item._id, {
    from: {
      opacity: 0,
      height: 0
    },
    enter: (item) => async (next) =>
      next({ opacity: 1, height: refMap.get(item).current.offsetHeight }),
    leave: (item) => async (next) => {
      await next({ opacity: 0 })
      await next({ height: 0 })
    },
    config: config.stiff
  })

  return (
    <NotificationsWrapper>
      {transitions.map(({ key, item, props }) => {
        refMap.set(item, React.createRef())
        return (
          <NotificationItem
            key={key}
            ref={refMap.get(item)}
            item={item}
            style={props}
            setNotifications={setNotifications}
          />
        )
      })}
    </NotificationsWrapper>
  )
}

Notifications.propTypes = {
  me: PropTypes.object.isRequired
}

export default Notifications
