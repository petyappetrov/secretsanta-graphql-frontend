
/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import { Link } from '@reach/router'
import { Text } from 'ui'

const NotificationWishHasBeenUpdated = ({ room }) =>
  <Text>
    Внимание! Ваш подопечный изменил своё пожелание к&nbsp;подарку.
    Перейти в&nbsp;<Link to={`/${room.slug}`}>команту</Link>
  </Text>

NotificationWishHasBeenUpdated.propTypes = {
  room: PropTypes.object.isRequired
}

export default NotificationWishHasBeenUpdated
