
/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import { Link } from '@reach/router'
import { Text } from 'ui'

const NotificationMemberSuccessAddedToRoom = ({ member, room }) =>
  <Text>
    В&nbsp;комнате <Link to={`/${room.slug}`}>{room.name}</Link> появился новый участник {member.name}!
  </Text>

NotificationMemberSuccessAddedToRoom.propTypes = {
  member: PropTypes.object.isRequired,
  room: PropTypes.object.isRequired
}

export default NotificationMemberSuccessAddedToRoom
