/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import { graphql, compose } from 'react-apollo'
import { withFormik, Form } from 'formik'
import styled from 'styled-components'
import { Button, Field, Box } from 'ui'
import { createValidate, isRequired } from 'helpers/validation'
import RoomQuery from 'schemas/queries/RoomQuery'
import MeQuery from 'schemas/queries/MeQuery'
import CreateWishMutation from 'schemas/mutations/CreateWishMutation'
import UpdateWishMutation from 'schemas/mutations/UpdateWishMutation'

const OptionsList = styled.div`
  display: flex;
  flex-wrap: wrap;
  margin: .5rem 0;
`

const Option = styled.div`
  padding: 0.5rem 1rem;
  border-radius: 20px;
  background-color: #CF4F5A;
  color: #fff;
  margin: 0 .5rem .5rem 0;
  cursor: pointer;
`

const options = [
  'На твоё усмотрение',
  'Абонемент в тренажёрный зал',
  'Интересную книгу',
  'Вкусняшки',
  'Настольную игру'
]

const WishForm = ({ wish, setFieldValue, onCancel }) =>
  <Form>
    <Field.Textarea
      label='Ваше пожелание'
      name='text'
      placeholder='Введите текст'
    />
    <OptionsList>
      {options.map((option, i) =>
        <Option
          key={i}
          onClick={() => setFieldValue('text', option)}
        >
          {option}
        </Option>
      )}
    </OptionsList>
    <Box mt={3}>
      <Button mr={2} view='secondary' onClick={onCancel}>Отмена</Button>
      <Button type='submit'>Сохранить</Button>
    </Box>
  </Form>

WishForm.propTypes = {
  wish: PropTypes.object,
  onCancel: PropTypes.func.isRequired,
  setFieldValue: PropTypes.func.isRequired
}

const validate = createValidate({
  text: [
    isRequired('Для участия нужно ввести ваше пожелание к подарку')
  ]
})

const enhance = compose(
  graphql(CreateWishMutation, { name: 'createWish' }),
  graphql(UpdateWishMutation, { name: 'updateWish' }),
  withFormik({
    validate,
    mapPropsToValues: (props) => ({ text: props.wish ? props.wish.text : '', room: props.room }),
    handleSubmit: (variables, { props, setErrors }) =>
      props[props.wish ? 'updateWish' : 'createWish']({
        variables,
        refetchQueries: [
          {
            query: RoomQuery,
            variables: {
              slug: props.slug
            }
          },
          {
            query: MeQuery
          }
        ]
      })
        .then(props.onCancel)
        .catch(({ graphQLErrors = [] }) =>
          graphQLErrors.map(err => err.data).forEach(setErrors))
  })
)

export default enhance(WishForm)
