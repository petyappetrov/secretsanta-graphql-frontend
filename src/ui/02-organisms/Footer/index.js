/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import styled from 'styled-components'
import gql from 'graphql-tag'
import { Trans } from '@lingui/macro'
import { useQuery } from 'react-apollo-hooks'

const FooterStyled = styled.footer`
  display: flex;
  flex-shrink: 0;
  justify-content: space-between;
  font-size: 11px;
  line-height: 18px;
  padding: 2rem 0;
  > div:last-child {
    text-align: right;
  }
`

const SECRET_SANTA_COUNT_QUERY = gql`{ count }`

const Footer = () => {
  const { data, loading } = useQuery(SECRET_SANTA_COUNT_QUERY, { fetchPolicy: 'cache-first' })
  return (
    <FooterStyled>
      <div>
        <Trans>Тайных Сант зерегистрировано: {loading ? <Trans>Загрузка...</Trans> : data.count}</Trans>
        <br />
        <Trans>По всем вопросам - <a href='/'>petyappetrov@yandex.ru</a></Trans>
      </div>
      <div>
        <Trans>Разработка сайта - <a href='/'>petyappetrov</a></Trans><br />
        <Trans>Все права защищены © 2018 – 2019</Trans>
      </div>
    </FooterStyled>
  )
}

export default Footer
