/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import styled from 'styled-components'
import { Link } from '@reach/router'
import { Trans } from '@lingui/macro'
import { Logo, Button } from 'ui'
import { useQuery, useMutation } from 'react-apollo-hooks'
import MeQuery from 'schemas/queries/MeQuery'
import SettingsQuery from 'schemas/queries/SettingsQuery'
import UpdateSettginsMutation from 'schemas/mutations/UpdateSettginsMutation'
import UpdateMemberMutation from 'schemas/mutations/UpdateMemberMutation'
import enIcon from './en.svg'
import ruIcon from './ru.svg'
import moonIcon from './moon.svg'
import sunIcon from './sun.svg'

const HeaderStyled = styled.header`
  padding: 40px 0 35px;
  display: flex;
  flex-shrink: 0;
  justify-content: space-between;
  flex-direction: column;
  align-items: flex-start;
  position: relative;
`

const HeaderTop = styled.div`
  width: 100%;
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
`

const Nav = styled.nav`
  display: flex;
  margin-top: 20px;
`

const MenuLink = styled(Link)`
  font-weight: bold;
  margin-right: 2rem;
  color: ${p => p.theme.text};
`

const InterfaceTogglers = styled.div`
  display: flex;
  align-items: center;
`

const LocaleToggler = styled.img`
  width: 26px;
  height: 26px;
  margin-left: 15px;
  cursor: pointer;
  ${p => p.disabled && `
    pointer-events: none;
    opacity: .5;
  `}
`

const Icon = styled.img`
  width: 12px;
  height: 12px;
`

const Header = () => {
  const { data } = useQuery(MeQuery, { fetchPolicy: 'cache-only' })
  const { data: { settings } } = useQuery(SettingsQuery)

  const updateMember = useMutation(UpdateMemberMutation)
  const updateSettings = useMutation(UpdateSettginsMutation)

  const toggle = (variables) => async () => {
    if (data.me) {
      await updateMember({ variables })
    }
    updateSettings({ variables })
  }

  const toggleTheme = toggle({
    theme: settings.theme === 'light' ? 'dark' : 'light'
  })

  const toggleLocale = toggle({
    locale: settings.locale === 'ru' ? 'en' : 'ru'
  })

  return (
    <HeaderStyled>
      <HeaderTop>
        <Logo />
        <InterfaceTogglers>
          <Button
            size='xs'
            view='secondary'
            onClick={toggleTheme}
            icon={<Icon src={settings.theme === 'light' ? sunIcon : moonIcon} />}
          >
            {settings.theme === 'light'
              ? <Trans>День</Trans>
              : <Trans>Ночь</Trans>
            }
          </Button>
          <LocaleToggler
            onClick={toggleLocale}
            src={settings.locale === 'ru' ? ruIcon : enIcon}
          />
        </InterfaceTogglers>
      </HeaderTop>
      {data.me
        ? (
          <Nav>
            <MenuLink to='/rooms'>
              <Trans>Мои комнаты</Trans>
            </MenuLink>
            <MenuLink to='/conversations'>
              <Trans>Сообщения</Trans>
            </MenuLink>
            <MenuLink to='/notifications'>
              <Trans>Уведомления</Trans>
            </MenuLink>
            <MenuLink to='/profile'>
              <Trans>Мой профиль</Trans>
            </MenuLink>
          </Nav>
        )
        : (
          <Nav>
            <MenuLink to='/login'>
              <Trans>Вход</Trans>
            </MenuLink>
            <MenuLink to='/register'>
              <Trans>Регистрация</Trans>
            </MenuLink>
          </Nav>
        )
      }
    </HeaderStyled>
  )
}

export default Header
