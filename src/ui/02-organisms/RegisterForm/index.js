/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import { Formik, Form, Field } from 'formik'
import { useApolloClient, useMutation } from 'react-apollo-hooks'
import { navigate } from '@reach/router'
import queryString from 'query-string'
import { Button, FormFooter, Label, Text, Box, Input } from 'ui'
import { createValidate, isRequired, isEmail, minLength } from 'helpers/validation'
import RegisterMemberMutation from 'schemas/mutations/RegisterMemberMutation'
import ExistEmailMutation from 'schemas/mutations/ExistEmailMutation'

const RegisterForm = ({ location }) => {
  const client = useApolloClient()
  const register = useMutation(RegisterMemberMutation)
  const existEmail = useMutation(ExistEmailMutation)

  const onSubmit = async (variables) => {
    const { data } = await register({ variables })

    localStorage.setItem('jwt', data.registerMember)
    await client.resetStore()

    const { room } = queryString.parse(location.search)
    if (room) {
      navigate(`/${room}`)
    } else {
      navigate('/')
    }
  }

  const emailExistValidate = async (email) => {
    if (!email) {
      return 'Пожалуйста введите почту'
    }
    const { data } = await existEmail({ variables: { email } })
    if (data.emailIsExist) {
      return 'Такой email уже используется'
    }
  }

  return (
    <Formik
      validate={createValidate({
        name: [
          isRequired('Пожалуйста введите имя')
        ],
        email: [
          isRequired('Пожалуйста введите почту'),
          isEmail()
        ],
        password: [
          isRequired('Пожалуйста введите пароль'),
          minLength(4)
        ]
      })}
      onSubmit={onSubmit}
      validateOnBlur
      initialValues={{
        name: '',
        email: '',
        password: '',
        passwordConfirm: ''
      }}
    >
      {({ errors, touched }) =>
        <Form>
          <Field name='name'>
            {({ field, form: { errors, touched } }) =>
              <Box mb={3}>
                <Label error={errors.name && touched.name}>Имя</Label>
                <Input type='text' placeholder='Введите имя' {...field} />
                {errors.name && touched.name &&
                  <Text error>{errors.name}</Text>
                }
              </Box>
            }
          </Field>
          <Field name='email' validate={emailExistValidate}>
            {({ field, form: { errors, touched } }) =>
              <Box mb={3}>
                <Label error={errors.email && touched.email}>Почта</Label>
                <Input type='email' placeholder='Введите почту' {...field} />
                {errors.email && touched.email &&
                  <Text error>{errors.email}</Text>
                }
              </Box>
            }
          </Field>
          <Field name='password'>
            {({ field, form: { errors, touched } }) =>
              <Box mb={3}>
                <Label error={errors.password && touched.password}>Пароль</Label>
                <Input type='password' placeholder='Придумайте пароль' {...field} />
                {errors.password && touched.password &&
                  <Text error>{errors.password}</Text>
                }
              </Box>
            }
          </Field>
          <FormFooter>
            <Button to='/' ml={3} view='secondary'>Отмена</Button>
            <Button type='submit' ml={2}>Регистрация</Button>
          </FormFooter>
        </Form>
      }
    </Formik>
  )
}

RegisterForm.propTypes = {
  location: PropTypes.object.isRequired
}

export default RegisterForm
