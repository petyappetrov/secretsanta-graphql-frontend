/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React, { useState } from 'react'
import PropTypes from 'prop-types'
import { useQuery, useSubscription, useMutation } from 'react-apollo-hooks'
import styled from 'styled-components'
import { useTransition, animated, config } from 'react-spring'
import { Button, Textarea, Spinner, Avatar, Section } from 'ui'
import MessageAddedSubscription from 'schemas/subscriptions/MessageAddedSubscription'
import ConversationQuery from 'schemas/queries/ConversationQuery'
import AddMessageMutation from 'schemas/mutations/AddMessageMutation'

const MessageGroup = styled.div`
  display: flex;
  align-items: flex-end;
  margin: 1rem 0 2.5rem;
  ${p => p.isMe && `
    flex-direction: row-reverse;
  `}
`

const Messages = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  box-sizing: border-box;
  ${p => p.isMe && `
    padding-right: 0;
    align-items: flex-end;
  `}
`

const Message = styled(animated.div)`
  display: flex;
  align-items: flex-end;
  &:not(:last-child) {
    margin-bottom: 10px;
  }
  ${p => p.isMe && `
    flex-direction: row-reverse;
  `}
`

const MessageAvatar = styled(Avatar)`
  align-items: flex-end;
`

const MessageAuthor = styled.div`
  margin-left: 15px;
  margin-bottom: 10px;
  font-size: 12px;
  opacity: .6;
`

const MessageText = styled.div`
  line-height: 1.618em;
  color: #fff;
  background: #11998e;
  border-radius: 20px;
  box-sizing: border-box;
  padding: .6rem 1rem;
  margin-right: 0.5rem;
  ${p => p.isMe && `
    color: #fff;
    border: none;
    background: #88B363;
    margin-right: 0;
    margin-left: 0.5rem;
  `}
`
const MessageDate = styled.div`
  opacity: .6;
  padding-bottom: 2px;
  font-size: 12px;
`

const ShowMoreButton = styled.div`
  margin: 1rem 0;
  color: #88B363;
  text-align: center;
  cursor: pointer;
  user-select: none;
`

const AddMessageStyled = styled.div`
  display: flex;
`

const TextareaStyled = styled(Textarea)`
  width: 100%;
  margin-right: .5rem;
  height: 60px;
  border-radius: 15px;
  padding-left: 16px;
  &::placeholder {
    color: ${p => p.theme.text};
    font-weight: 700;
  }
  &:focus {
    &::placeholder {
      opacity: .4;
    }
  }
`

const AddMessage = ({ conversation }) => {
  const [text, setText] = useState('')
  const addMessage = useMutation(AddMessageMutation, {
    variables: {
      text,
      conversation
    }
  })
  return (
    <AddMessageStyled>
      <TextareaStyled
        placeholder='Напишите что-нибудь'
        value={text}
        onChange={(event) => setText(event.target.value)}
      />
      <Button
        view='secondary'
        onClick={() => addMessage().then(() => setText(''))}
      >
        Отправить
      </Button>
    </AddMessageStyled>
  )
}

AddMessage.propTypes = {
  conversation: PropTypes.string.isRequired
}

const MessagesUser = ({ messages, isMe }) => {
  const transitions = useTransition(messages, (item) => item._id, {
    from: {
      opacity: 0,
      transform: 'translate3d(0, 20px, 0)'
    },
    enter: (item) => async (next) => next({
      opacity: 1,
      transform: 'translate3d(0, 0px, 0)'
    }),
    update: (item) => async (next) => {
      await next({
        transform: 'translate3d(0, 0px, 0)'
      })
    },
    unique: true,
    config: config.gentle
  })
  return (
    <div>
      {transitions.map(({ item, key, props }, i) => {
        return (
          <Message style={props} key={key} isMe={isMe}>
            <div>
              <MessageText isMe={isMe}>{item.text}</MessageText>
              {messages.length - 1 === i &&
                <MessageDate>{item.createdAt}</MessageDate>
              }
            </div>
          </Message>
        )
      })}
    </div>
  )
}

const Conversation = ({ conversation, me }) => {
  const { data, loading, fetchMore } = useQuery(ConversationQuery, {
    variables: {
      conversation,
      limit: 6,
      skip: 0
    }
  })

  useSubscription(MessageAddedSubscription, {
    variables: { conversation },
    onSubscriptionData: ({ subscriptionData, client }) => {
      const data = client.readQuery({
        query: ConversationQuery,
        variables: {
          conversation,
          limit: 6,
          skip: 0
        }
      })
      client.writeQuery({
        query: ConversationQuery,
        variables: {
          conversation,
          limit: 6,
          skip: 0
        },
        data: {
          ...data,
          conversation: {
            ...data.conversation,
            messages: {
              ...data.conversation.messages,
              count: data.conversation.messages.count + 1,
              items: [
                ...data.conversation.messages.items,
                subscriptionData.data.messageAdded
              ]
            }
          }
        }
      })
    }
  })

  if (loading) {
    return <Spinner />
  }

  if (!data.conversation.messages) {
    return null
  }

  const groupedMessages = data.conversation.messages.items.reduce((prev, item, key, array) => {
    const prevItem = prev[prev.length - 1]
    if (!prevItem) {
      return [
        {
          authorId: item.author._id,
          items: [item]
        }
      ]
    }

    if (item.author._id === prevItem.authorId) {
      prevItem.items.push(item)
      return prev
    }

    return [
      ...prev,
      {
        authorId: item.author._id,
        items: [item]
      }
    ]
  }, [])

  const onLoadMore = () => {
    fetchMore({
      variables: {
        skip: data.conversation.messages.items.length
      },
      updateQuery: (data, { fetchMoreResult }) => {
        if (!fetchMoreResult) {
          return data
        }
        return {
          ...data,
          conversation: {
            ...data.conversation,
            messages: {
              ...data.conversation.messages,
              items: [
                ...fetchMoreResult.conversation.messages.items,
                ...data.conversation.messages.items
              ]
            }
          }
        }
      }
    })
  }

  return (
    <Section title='Общий чат' withToggler>
      {data.conversation.messages.count > data.conversation.messages.items.length && (
        <ShowMoreButton onClick={onLoadMore}>
          Показать предыдущие сообщения
        </ShowMoreButton>
      )}
      {groupedMessages.map(({ items, authorId }, i) => {
        const isMe = me._id === authorId
        return (
          <MessageGroup key={authorId + i} isMe={isMe}>
            <MessageAvatar size={55} alt={items[0].author.name} />
            <Messages isMe={isMe}>
              {me._id !== authorId &&
                <MessageAuthor>{items[0].author.name}</MessageAuthor>
              }
              <MessagesUser isMe={isMe} messages={items} />
            </Messages>
          </MessageGroup>
        )
      })}
      <AddMessage conversation={conversation} />
    </Section>
  )
}

Conversation.propTypes = {
  conversation: PropTypes.string.isRequired,
  me: PropTypes.object.isRequired
}

export default Conversation
