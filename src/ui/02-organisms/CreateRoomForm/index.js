/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import { useMutation } from 'react-apollo-hooks'
import { Formik, Form } from 'formik'
import { navigate } from '@reach/router'
import { Field, Button, Box, Label, Input, Text, Textarea } from 'ui'
import { createValidate, isRequired } from 'helpers/validation'
import CreateRoomMutation from 'schemas/mutations/CreateRoomMutation'
import MeQuery from 'schemas/queries/MeQuery'

const updateCache = (client, { data: { createRoom } }) => {
  const data = client.readQuery({ query: MeQuery })
  client.writeQuery({
    query: MeQuery,
    data: {
      me: {
        ...data.me,
        rooms: [
          ...data.me.rooms,
          createRoom
        ]
      }
    }
  })
}

const CreateRoomForm = () => {
  const createRoomMutate = useMutation(CreateRoomMutation)

  const onSubmit = async (variables) => {
    const { data } = await createRoomMutate({
      variables,
      update: updateCache
    })
    navigate(data.createRoom.slug)
  }

  return (
    <Formik
      validate={createValidate({
        name: [
          isRequired('Пожалуйста введите название комнаты')
        ],
        sum: [
          isRequired('Пожалуйста введите стоимость подарка')
        ]
      })}
      onSubmit={onSubmit}
      validateOnBlur
      initialValues={{ name: '', description: '', sum: '' }}
    >
      <Form>
        <Field name='name'>
          {({ field, form: { errors, touched } }) =>
            <Box mb={3}>
              <Label error={errors.name && touched.name}>Название</Label>
              <Input type='text' placeholder='Введите название' {...field} />
              {errors.name && touched.name &&
                <Text error>{errors.name}</Text>
              }
            </Box>
          }
        </Field>
        <Field name='sum'>
          {({ field, form: { errors, touched } }) =>
            <Box mb={3}>
              <Label error={errors.sum && touched.sum}>Максимальная стоимость подарка</Label>
              <Input type='number' placeholder='Введите стоимость' {...field} />
              {errors.sum && touched.sum &&
                <Text error>{errors.sum}</Text>
              }
            </Box>
          }
        </Field>
        <Field name='description'>
          {({ field }) =>
            <Box mb={3}>
              <Label>Описание</Label>
              <Textarea placeholder='Введите описание' {...field} />
            </Box>
          }
        </Field>
        <Box mt={3}>
          <Button view='secondary' to='/' mr={2}>Отмена</Button>
          <Button type='submit'>Организовать игру</Button>
        </Box>
      </Form>
    </Formik>
  )
}

export default CreateRoomForm
