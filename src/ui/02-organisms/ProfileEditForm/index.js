/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import { withFormik, Form } from 'formik'
import { graphql } from 'react-apollo'
import { compose } from 'recompose'
import { Field, Button, Box } from 'ui'
import { createValidate, isRequired } from 'helpers/validation'
import UpdateMemberMutation from 'schemas/mutations/UpdateMemberMutation'
import MeQuery from 'schemas/queries/MeQuery'

const EditProfileForm = ({ onCancel }) =>
  <Form>
    <Field.Input
      label='Имя'
      name='name'
      placeholder='Введите имя'
    />
    <Field.Input
      label='Почта'
      name='email'
      placeholder='Введите почту'
    />
    <Field.Input
      label='Пароль'
      name='password'
      placeholder='Придумайте пароль'
    />
    <Box mt={4}>
      <Button view='secondary' onClick={onCancel} mr={2}>Отмена</Button>
      <Button type='submit'>Сохранить</Button>
    </Box>
  </Form>

EditProfileForm.propTypes = {
  onCancel: PropTypes.func.isRequired
}

const validate = createValidate({
  name: [
    isRequired('Пожалуйста введите имя')
  ],
  email: [
    isRequired('Пожалуйста введите почту')
  ]
})

const updateCache = (client, { data: { updateMember } }) => {
  const data = client.readQuery({ query: MeQuery })
  client.writeQuery({
    query: MeQuery,
    data: {
      me: {
        ...data.me,
        ...updateMember
      }
    }
  })
}

const enhance = compose(
  graphql(UpdateMemberMutation),
  withFormik({
    validate,
    mapPropsToValues: ({ name, email }) => ({
      name,
      email,
      password: ''
    }),
    handleSubmit: (variables, { props, setErrors, client }) =>
      props.mutate({
        variables,
        update: updateCache
      })
        .then((response) => {
          if (typeof props.afterSubmit === 'function') {
            props.afterSubmit(response)
          }
        })
        .catch(({ graphQLErrors = [] }) =>
          graphQLErrors.map(err => err.data).forEach(setErrors))
  })
)

export default enhance(EditProfileForm)
