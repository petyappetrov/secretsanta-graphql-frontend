/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import { Formik, Form, Field } from 'formik'
import { useApolloClient, useMutation } from 'react-apollo-hooks'
import { navigate } from '@reach/router'
import { Button, FormFooter, Label, Text, Box, Input } from 'ui'
import { createValidate, isRequired, isEmail, minLength } from 'helpers/validation'
import LoginMemberMutation from 'schemas/mutations/LoginMemberMutation'

const LoginForm = () => {
  const client = useApolloClient()
  const login = useMutation(LoginMemberMutation)

  const onSubmit = async (variables) => {
    const { data } = await login({ variables })

    localStorage.setItem('jwt', data.loginMember)
    await client.resetStore()

    navigate('/profile')
  }
  return (
    <Formik
      validate={createValidate({
        email: [
          isRequired('Пожалуйста введите почту'),
          isEmail()
        ],
        password: [
          isRequired('Пожалуйста введите пароль'),
          minLength(4)
        ]
      })}
      onSubmit={onSubmit}
      validateOnBlur
      initialValues={{
        email: '',
        password: ''
      }}
    >
      <Form>
        <Field name='email'>
          {({ field, form: { errors, touched } }) =>
            <Box mb={3}>
              <Label error={errors.email && touched.email}>Почта</Label>
              <Input type='email' placeholder='Введите почту' {...field} />
              {errors.email && touched.email &&
                <Text error>{errors.email}</Text>
              }
            </Box>
          }
        </Field>
        <Field name='password'>
          {({ field, form: { errors, touched } }) =>
            <Box mb={3}>
              <Label error={errors.password && touched.password}>Пароль</Label>
              <Input type='password' placeholder='Введите пароль' {...field} />
              {errors.password && touched.password &&
                <Text error>{errors.password}</Text>
              }
            </Box>
          }
        </Field>
        <FormFooter>
          <Button to='/' ml={3} view='secondary'>Отмена</Button>
          <Button type='submit' ml={2}>Войти</Button>
        </FormFooter>
      </Form>
    </Formik>

  )
}

export default LoginForm
