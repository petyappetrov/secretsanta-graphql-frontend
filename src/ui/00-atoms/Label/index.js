/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import styled from 'styled-components'

const Label = styled.label`
  display: block;
  margin-bottom: 8px;
  font-weight: 600;
  text-align: left;
  font-size: 14px;
  line-height: 18px;
  color: ${p => p.theme.text};
  ${(p) => p.error && 'color: #CF4F5A;'}
`

export default Label
