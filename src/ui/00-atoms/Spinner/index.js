/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import styled, { keyframes } from 'styled-components'

const stretchdelay = keyframes`
  0%, 40%, 100% {
    transform: scaleY(0.4);
  }  20% {
    transform: scaleY(1.0);
  }
`

const Spinner = styled.span`
  display: inline-block;
  width: 50px;
  height: 40px;
  text-align: center;
  font-size: 10px;
  & > div {
    background-color: ${p => p.theme.text};
    height: 100%;
    width: 6px;
    margin: 0 3px 0 0;
    display: inline-block;
    animation: ${stretchdelay} 1.2s infinite ease-in-out;
  }
  & > div:nth-child(2) {
    animation-delay: -1.1s;
  }

  & > div:nth-child(3) {
    animation-delay: -1.0s;
  }

  & > div:nth-child(4) {
    animation-delay: -0.9s;
  }

  & > div:nth-child(5) {
    animation-delay: -0.8s;
  }
`

export default () =>
  <Spinner>
    <div />
    <div />
    <div />
    <div />
    <div />
  </Spinner>
