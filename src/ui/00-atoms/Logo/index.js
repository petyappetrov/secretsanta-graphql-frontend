/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import { Link } from '@reach/router'
import styled from 'styled-components'
import { Trans } from '@lingui/macro'

const LogoLink = styled(Link)`
  font-size: 2.5rem;
  font-weight: 700;
  color: ${p => p.theme.logo};
  font-family: 'Lobster', cursive;
  &:hover {
    text-decoration: none;
  }
`

const Logo = () =>
  <LogoLink to='/'>
    <Trans>Тайный Санта</Trans>
  </LogoLink>

export default Logo
