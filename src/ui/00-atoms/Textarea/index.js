/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import styled from 'styled-components'

const Textarea = styled.textarea`
  width: 100%;
  height: 140px;
  line-height: 40px;
  font-weight: 700;
  font-size: 1em;
  padding: .1rem 1em calc(.78571429em - 2px);
  background: transparent;
  border: 1px solid #ebedf0;
  resize: none;
  color: ${p => p.theme.text};
  border-radius: 6px;
  box-sizing: border-box;
  background: transparent;
  &::placeholder {
    color: ${p => p.theme.text};
    font-weight: 700;
    opacity: .8;
  }
  &:focus {
    &::placeholder {
      opacity: .4;
    }
  }
  ${p => p.error && `
    border-color: #CF4F5A;
    &::placeholder {
      color: #CF4F5A;
    }
  `}
`

export default Textarea
