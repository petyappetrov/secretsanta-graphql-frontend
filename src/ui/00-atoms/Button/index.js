/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import styled, { css } from 'styled-components'
import { Link } from '@reach/router'
import { Trans } from '@lingui/macro'
import space from 'helpers/space'

const views = {
  default: `
    background-color: #88B363;
    color: #fff;
    &:hover {
      background-color: #92C269;
    }
  `,
  secondary: css`
    background-color: ${p => p.theme.buttons.secondary.bg};
    color: ${p => p.theme.buttons.secondary.text};
    &:hover {
      background-color: ${p => p.theme.buttons.secondary.hover};
    }
  `,
  optional: `
    background: transparent;
    color: #fff;
    border: 2px solid #fff;
    line-height: 36px;
    &:hover {
      background-color: #fff;
      color: #000;
    }
  `,
  danger: `
    background-color: #CF4F5A;
    color: #fff;
    &:hover {
      background-color: #E4535F;
    }
  `,
  link: css`
    background-color: transparent;
    color: ${p => p.theme.text};
  `
}

const sizes = {
  xs: `
    padding: 0 14px;
    height: 26px;
    line-height: 26px;
    font-size: 11px;
  `,
  sm: css`
    padding: 0 22px;
    height: 40px;
    line-height: 40px;
    text-transform: uppercase;
    ${p => p.view === 'optional' && `
      line-height: 36px;
    `}
  `,
  lg: `
    padding: 0 30px;
    height: 50px;
    line-height: 50px;
  `
}

const buttonCSS = css`
  ${space}
  white-space: nowrap;
  word-break: keep-all;
  display: inline-flex;
  flex-shrink: 0;
  flex-grow: 0;
  align-items: center;
  font-size: 14px;
  border-radius: 20px;
  box-sizing: border-box;
  border: none;
  cursor: pointer;
  user-select: none;
  font-weight: bold;
  outline: none;
  text-decoration: none;
  width: ${p => (p.fullWidth ? '100%' : 'auto')};
  &:first-child {
    margin-left: 0;
  }
  &:last-child {
    margin-right: 0;
  }
  &:disabled {
    opacity: 0.55;
    pointer-events: none;
    cursor: default;
  }
  ${(p) => views[p.view]}
  ${(p) => sizes[p.size]}
`

const ButtonStyled = styled.button`
  ${buttonCSS}
`

const LinkStyled = styled(Link)`
  ${buttonCSS}
  ${p => p.to && `
    &:hover {
      text-decoration: none;
    }
  `}
`

const Icon = styled.span`
  display: flex;
  width: 12px;
  margin-right: 6px;
  margin-left: -4px;
`

const Button = ({
  view = 'default',
  size = 'sm',
  type = 'button',
  isLoading = false,
  disabled = false,
  children,
  icon,
  ...props
}) => {
  const Component = props.to ? LinkStyled : ButtonStyled
  return (
    <Component
      {...props}
      view={view}
      size={size}
      type={type}
      disabled={isLoading || disabled}
    >
      {isLoading
        ? <Trans>Загрузка...</Trans>
        : (
          <>
            {icon && <Icon>{icon}</Icon>}
            {children}
          </>
        )}
    </Component>
  )
}

Button.propTypes = {
  onClick: PropTypes.func,
  view: PropTypes.oneOf(['default', 'danger', 'secondary', 'optional', 'link']),
  size: PropTypes.oneOf(['xs', 'sm', 'lg']),
  to: PropTypes.string,
  type: PropTypes.string,
  isLoading: PropTypes.bool,
  disabled: PropTypes.bool,
  children: PropTypes.node,
  icon: PropTypes.node
}

export default Button
