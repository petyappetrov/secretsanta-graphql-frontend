/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import styled from 'styled-components'
import space from 'helpers/space'

const Box = styled.div`
  width: 100%;
  ${space};
`

export default Box
