/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import styled from 'styled-components'

const Text = styled.div`
  line-height: 1.618em;
  ${p => p.center && 'text-align: center'};
  ${p => p.size && `font-size: ${p.size}`};
  ${p => p.error && 'color: #CF4F5A'};
  ${p => p.color ? `color: ${p.color}` : ''};
`

export default Text
