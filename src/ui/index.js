/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

const req = require.context('.', true, /\.\/[^/]+\/[^/]+\/index\.js$/)
req.keys().forEach((key) => {
  const keys = key.split('/')
  const componentName = keys[keys.length - 2]
  if (componentName) {
    module.exports[componentName] = req(key).default
  }
})
