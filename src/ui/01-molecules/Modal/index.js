/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React, { useState, useEffect, useCallback } from 'react'
import { createPortal } from 'react-dom'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { useTransition, animated, config } from 'react-spring'

const Overlay = styled(animated.div)`
  position: fixed;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  display: flex;
  justify-content: center;
  background-color: ${p => p.theme.backgroundColor};
  cursor: pointer;
  z-index: 11000;
`

const Content = styled(animated.div)`
  box-shadow: ${p => p.theme.shadow};
  margin-top: 160px;
  margin-bottom: auto;
  flex: 0 1 auto;
  width: 640px;
  cursor: default;
`

const Modal = ({ children }) => {
  const [isOpen, setOpen] = useState(false)
  const [element] = useState(document.createElement('div'))

  useEffect(() => {
    document.body.appendChild(element)
    return () => document.body.removeChild(element)
  }, [])

  const transitions = useTransition(isOpen, null, {
    from: {
      opacity: 0,
      transform: 'translate3d(0, -40px, 0)'
    },
    enter: {
      opacity: 1,
      transform: 'translate3d(0, 0, 0)'
    },
    leave: {
      opacity: 0,
      transform: 'translate3d(0, 40px, 0)'
    },
    config: config.stiff
  })

  const openModal = useCallback(() => setOpen(true), [isOpen])
  const closeModal = useCallback(() => setOpen(false), [isOpen])

  return children({
    closeModal,
    openModal,
    content: (content) => createPortal(
      transitions.map(({ item, key, props }) => (
        item && <Overlay style={{ opacity: props.opacity }} key={key}>
          <Content style={{ transform: props.transform }}>
            {content}
          </Content>
        </Overlay>
      )),
      element
    )
  })
}

Modal.propTypes = {
  children: PropTypes.func.isRequired
}

export default Modal
