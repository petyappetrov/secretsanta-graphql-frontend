/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import styled from 'styled-components'
import { find } from 'lodash'
import gradients from './gradients'

const Gradient = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-start;
  padding: 36px;
  box-sizing: border-box;
  overflow: hidden;
  border-radius: 15px;
  min-height: ${p => p.height || 160}px;
  margin-bottom: 20px;
  background: ${p => {
    const { colors } = find(gradients, { name: p.name })
    return `linear-gradient(to right, ${colors[0]}, ${colors[1]})`
  }};
`

Gradient.defaultProps = {
  name: 'Sweet Dream'
}

Gradient.Title = styled.div`
  font-size: 22px;
  font-weight: bold;
  color: #fff;
  margin-bottom: 10px;
`

export default Gradient
