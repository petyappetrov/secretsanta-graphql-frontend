/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import { Field } from 'formik'
import styled from 'styled-components'
import { Input, Text, Label, Textarea } from 'ui'

const InputGroup = styled.div`
  margin-bottom: 20px;
`

Field.Input = ({ name, label, validate, ...props }) => {
  const field = {
    name
  }
  if (typeof validate === 'function') {
    field.validate = validate
  }
  return (
    <Field
      {...field}
      render={({
        field,
        form: {
          touched,
          errors
        }
      }) =>
        <InputGroup>
          {
            label &&
              <Label
                htmlFor={name}
                error={touched[name] && errors[name]}
              >
                {label}
              </Label>
          }
          <Input
            name={name}
            error={touched[name] && errors[name]}
            {...field}
            {...props}
          />
          {
            touched[name] && errors[name] &&
              <Text error>{touched[name] && errors[name]}</Text>
          }
        </InputGroup>
      }
    />
  )
}

Field.Input.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string,
  validate: PropTypes.func
}

Field.Textarea = ({ name, label, ...props }) =>
  <Field
    name={name}
    render={({
      field,
      form: {
        touched,
        errors
      }
    }) =>
      <InputGroup>
        {
          label &&
            <Label
              htmlFor={name}
              error={touched[name] && errors[name]}
            >
              {label}
            </Label>
        }
        <Textarea
          name={name}
          error={touched[name] && errors[name]}
          {...field}
          {...props}
        />
        {
          touched[name] && errors[name] &&
            <Text error>{touched[name] && errors[name]}</Text>
        }
      </InputGroup>
    }
  />

Field.Textarea.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string
}

export default Field
