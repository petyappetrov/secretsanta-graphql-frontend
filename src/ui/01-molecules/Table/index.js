/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { get } from 'helpers'

const Col = styled.td`
  padding: 1rem 1rem 1rem 0;
`

const Table = ({ data, columns }) =>
  <table width='100%'>
    <thead align='left'>
      <tr>
        {columns.map((column, i) =>
          <th
            key={i}
            width={column.width || 'auto'}
            height='20'
          >
            <strong>{column.header}</strong>
          </th>
        )}
      </tr>
    </thead>
    <tbody>
      {data.map((row) =>
        <tr key={row._id}>
          {columns.map((col, i) =>
            <Col key={i}>
              {typeof col.transform === 'function'
                ? col.transform(get(row, col.accessor), row)
                : get(row, col.accessor)
              }
            </Col>
          )}
        </tr>
      )}
    </tbody>
  </table>

Table.propTypes = {
  data: PropTypes.array.isRequired,
  columns: PropTypes.arrayOf(PropTypes.shape({
    header: PropTypes.oneOfType([PropTypes.string, PropTypes.node]).isRequired,
    accessor: PropTypes.string.isRequired,
    transform: PropTypes.func,
    width: PropTypes.oneOfType([
      PropTypes.number,
      PropTypes.string
    ])
  })).isRequired
}

export default Table
