
/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import { Link } from '@reach/router'
import { Text } from 'ui'

const NotificationMemberSuccessRegister = () =>
  <Text>
    Привет! С наступающим новым годом!<br />
    Организуй свою <Link to='/create-room'>первую игру</Link> "Тайный Санта".
  </Text>

export default NotificationMemberSuccessRegister
