
/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import { Link } from '@reach/router'
import { Text } from 'ui'

const NotificationRoomSuccessfulCreated = ({ room }) =>
  <Text>
    Комната <Link to={`/${room.slug}`}>{room.name}</Link> была успешна создана
  </Text>

NotificationRoomSuccessfulCreated.propTypes = {
  room: PropTypes.object.isRequired
}

export default NotificationRoomSuccessfulCreated
