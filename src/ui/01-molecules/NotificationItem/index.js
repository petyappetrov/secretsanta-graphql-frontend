/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React, { useEffect } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { useMutation } from 'react-apollo-hooks'
import MarkNotificationReadMutation from 'schemas/mutations/MarkNotificationReadMutation'

import NotificationMemberSuccessAddedToRoom from './NotificationMemberSuccessAddedToRoom'
import NotificationYouSuccessAddedToRoom from './NotificationYouSuccessAddedToRoom'
import NotificationRoomWasTossed from './NotificationRoomWasTossed'
import NotificationWishHasBeenUpdated from './NotificationWishHasBeenUpdated'
import NotificationRoomSuccessfulCreated from './NotificationRoomSuccessfulCreated'
import NotificationMemberSuccessRegister from './NotificationMemberSuccessRegister'

const notificationTexts = {
  NotificationMemberSuccessAddedToRoom,
  NotificationYouSuccessAddedToRoom,
  NotificationRoomWasTossed,
  NotificationWishHasBeenUpdated,
  NotificationRoomSuccessfulCreated,
  NotificationMemberSuccessRegister
}

const NotificationItemStyled = styled.div.attrs(props => ({
  style: {
    transform: `translate3d(${props.x}px, 0, 0)`,
    opacity: props.opacity
  }
}))`
  cursor: pointer;
  background-color: ${p => p.theme.backgroundColor};
  color: ${p => p.theme.text};
  border: 1px solid #ebedf0;
  line-height: 1.618;
  font-weight: bold;
  padding: 30px;
  margin-bottom: 10px;
  border-radius: 15px;
`

const NotificationItem = ({ closeNotification, data, style }) => {
  const markRead = useMutation(MarkNotificationReadMutation, {
    variables: {
      notification: data._id
    }
  })
  useEffect(() => {
    const timer = setTimeout(() => closeNotification(data._id), 4000)
    return () => clearTimeout(timer)
  }, [])

  const NotificationText = notificationTexts[data.__typename]
  return (
    <NotificationItemStyled
      {...style}
      onClick={() => markRead().then(() => closeNotification(data._id))}
    >
      <NotificationText {...data} />
    </NotificationItemStyled>
  )
}

NotificationItem.propTypes = {
  closeNotification: PropTypes.func.isRequired,
  data: PropTypes.object.isRequired,
  style: PropTypes.object.isRequired
}

NotificationItem.texts = notificationTexts

export default NotificationItem
