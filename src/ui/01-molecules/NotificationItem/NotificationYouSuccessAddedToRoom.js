
/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import { Link } from '@reach/router'
import { Text } from 'ui'

const NotificationMemberSuccessAddedToRoom = ({ room }) =>
  <Text>
    Поздавляем! Вы&nbsp;теперь участник комнаты <Link to={`/${room.slug}`}>{room.name}</Link>
  </Text>

NotificationMemberSuccessAddedToRoom.propTypes = {
  room: PropTypes.object.isRequired
}

export default NotificationMemberSuccessAddedToRoom
