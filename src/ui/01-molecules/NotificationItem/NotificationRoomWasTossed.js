
/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import { Link } from '@reach/router'
import { Text } from 'ui'

const NotificationRoomWasTossed = ({ room }) =>
  <Text>
    Ура! У&nbsp;вас появился подопечный.<br />
    Для того, чтобы посмотреть кто это, перейдите в&nbsp;<Link to={`/${room.slug}`}>комнату</Link> и&nbsp;нажмите
    на&nbsp;кнопку &laquo;Мой подопечный&raquo;
  </Text>

NotificationRoomWasTossed.propTypes = {
  room: PropTypes.object.isRequired
}

export default NotificationRoomWasTossed
