/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import santa from './santa-claus.svg'

const AvatarWrapper = styled.div`
  border-radius: 50%;
  overflow: hidden;
  width: ${p => p.size}px;
  height: ${p => p.size}px;
  display: flex;
  justify-content: center;
  align-items: center;
  flex-shrink: 0;
  > img {
    width: 80%;
    object-fit: cover;
  }
`

const Avatar = ({ src, alt, size = 100, className = '' }) =>
  <AvatarWrapper size={size} className={className}>
    <img src={src ? process.env.GRAPHQL_URI + '/' + src : santa} alt={alt} />
  </AvatarWrapper>

Avatar.propTypes = {
  src: PropTypes.string,
  className: PropTypes.string,
  size: PropTypes.number,
  alt: PropTypes.string.isRequired
}

export default Avatar
