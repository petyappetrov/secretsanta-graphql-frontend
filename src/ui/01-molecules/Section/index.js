/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

import React, { useState, useRef } from 'react'
import PropTypes from 'prop-types'
import { Trans } from '@lingui/macro'
import styled from 'styled-components'
import { useTransition, animated, config } from 'react-spring'

const Wrapper = styled.div`
  margin-bottom: 20px;
`

const Title = styled.div`
  font-size: 18px;
  border: 1px solid #ebedf0;
  border-radius: 15px;
  padding: 24px 36px;
  display: flex;
  align-items: center;
  justify-content: space-between;
  color: ${p => p.theme.text};
  transition: all 200ms ease;
  ${p => p.isOpen && `
    border-bottom: none;
    border-bottom-left-radius: 0;
    border-bottom-right-radius: 0;
  `}
`

const Content = styled(animated.div)`
  box-shadow: inset 0 0 0 1px #ebedf0;
  border-bottom-left-radius: 15px;
  border-bottom-right-radius: 15px;
`

const InnerContent = styled.div`
  padding: 34px 36px 40px;
`

const HideButton = styled.span`
  font-size: 14px;
  cursor: pointer;
  user-select: none;
`

const getHeight = (el) => {
  if (!el.current) {
    return 0
  }
  return el.current.offsetHeight
}

const Section = ({
  title,
  children,
  withToggler = false,
  opened = true
}) => {
  const [isOpen, setOpen] = useState(opened)
  const el = useRef(null)

  const transitions = useTransition(isOpen, null, {
    initial: null,
    from: {
      opacity: 0,
      height: 0
    },
    enter: () => async (next) => next({
      height: getHeight(el),
      opacity: 1
    }),
    update: () => async (next) => next({ height: getHeight(el) }),
    leave: {
      opacity: 0,
      height: 0
    },
    unique: true,
    reset: true,
    config: config.stiff
  })

  return (
    <Wrapper>
      <Title isOpen={isOpen}>
        <strong>{title}</strong>
        {withToggler && (
          <HideButton onClick={() => setOpen(!isOpen)}>
            {isOpen
              ? <Trans>Свернуть</Trans>
              : <Trans>Развернуть</Trans>
            }
          </HideButton>
        )}
      </Title>
      {transitions.map(({ item, props, key }) => {
        return item && (
          <Content style={props} key={key}>
            <InnerContent ref={el}>
              {children}
            </InnerContent>
          </Content>
        )
      })}
    </Wrapper>
  )
}

Section.propTypes = {
  title: PropTypes.oneOfType([PropTypes.string, PropTypes.node]),
  children: PropTypes.node,
  withToggler: PropTypes.bool,
  opened: PropTypes.bool
}

export default Section
