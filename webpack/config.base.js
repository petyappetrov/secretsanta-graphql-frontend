/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

const webpack = require('webpack')
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const HtmlWebpackTemplate = require('html-webpack-template')
const HtmlWebpackHarddiskPlugin = require('html-webpack-harddisk-plugin')

module.exports = {
  output: {
    filename: 'js/[name]-[hash].js',
    path: path.resolve(__dirname, '../dist/'),
    publicPath: '/'
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        use: {
          loader: 'babel-loader',
          options: {
            cacheDirectory: true,
            plugins: ['react-hot-loader/babel']
          }
        },
        exclude: /node_modules/
      },
      {
        test: /\.(graphql|gql)$/,
        exclude: /node_modules/,
        loader: 'graphql-tag/loader'
      },
      {
        test: /\.(png|jpg|jpeg|gif|svg)$/,
        use: [
          {
            loader: 'file-loader?name=img/[name]-[hash].[ext]'
          }
        ]
      }
    ]
  },

  plugins: [
    new webpack.EnvironmentPlugin(['NODE_ENV', 'PORT', 'GRAPHQL_URI', 'GRAPHQL_WS']),
    new HtmlWebpackPlugin({
      inject: false,
      template: HtmlWebpackTemplate,
      title: 'Тайный санта',
      appMountId: 'root',
      alwaysWriteToDisk: true,
      mobile: true,
      lang: 'ru',
      links: [
        'https://fonts.googleapis.com/css?family=Montserrat:400,700&amp;subset=cyrillic',
        'https://fonts.googleapis.com/css?family=Lobster'
      ]
    }),
    new HtmlWebpackHarddiskPlugin()
  ],

  resolve: {
    extensions: ['.wasm', '.mjs', '.js', '.json', '.gql'],
    modules: [
      'node_modules',
      path.join(__dirname, '../src')
    ]
  }
}
