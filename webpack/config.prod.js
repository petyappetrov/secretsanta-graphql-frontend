/**
 * @author    Petya Petrov <petyappetrov@yandex.ru>
 * @copyright Copyright (c) 2018
 * @license   ISC
 */

const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin
const merge = require('webpack-merge')
const baseConfig = require('./config.base')

const config = merge(baseConfig, {
  mode: 'production',
  entry: {
    app: './src/client.js',
    vendors: [
      'react',
      'react-dom'
    ]
  },
  optimization: {
    splitChunks: {
      automaticNameDelimiter: '-',
      chunks: 'all'
    }
  },
  plugins: [
    new BundleAnalyzerPlugin()
  ]
})

module.exports = config
